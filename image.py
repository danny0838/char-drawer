#!/usr/bin/python
# coding: UTF-8
def getStringImage(text, font, size=40, color=[0,0,0]):
    import Image, ImageDraw, ImageFont
    img = Image.new("RGBA", (size, size), "white")
    draw = ImageDraw.Draw( img )
    draw.ink = color[0] + color[1]*256 + color[2]*256*256
    font = ImageFont.truetype(font, size)
    draw.text( (0,0), text, font=font )

    data = img.getdata()
    newData = list()
    for item in data:
        if (item[0], item[1], item[2]) == (255, 255, 255):
            newData.append((255, 255, 255, 0))
        else:
            newData.append((item[0], item[1], item[2], 255))
    img.putdata(newData)
    return img

text = ""  # U+E010
font = "hzcdp01b.ttf"
size = 100
color = [0, 0, 0]

img = getStringImage( text.decode('UTF-8'), font, size, color )
print 'Content-Type: image/png'
print 'Content-Disposition: inline; filename="image.png"'
print
import cStringIO
f = cStringIO.StringIO()
img.save(f, "png")
f.seek(0)
print f.read()